/*
***************************************************************************************************
*
*   FileName : eflash_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_EFLASH_TEST_HEADER
#define MCU_BSP_EFLASH_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_EFLASH == 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_EFLASH != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_EFLASH value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_EFLASH != 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_FMU != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_FMU value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_FMU != 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_SFMC != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_SFMC value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_SFMC != 1 )

#include <sal_internal.h>


/*
***************************************************************************************************
*                                             DEFINITIONS
***************************************************************************************************
*/


/*
***************************************************************************************************
*                                             LOCAL VARIABLES
***************************************************************************************************
*/

/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/


/*
***************************************************************************************************
*                                       EFLASHTest_ECC
*
* EFLASH ECC test
*
* @param    none
*
* @return   none
*
* Notes
*           [SM-PV] 05. Data eFLASH and Controller - DataFlash.01, ECC (Error Correcting Code)
*           Flash memory must be able to detect 1-bit or 2-bit data errors occurring in flash
*           memory through ECC.
***************************************************************************************************
*/
void EFLASHTest_ECC
(
    uint32 uiFlag
);

/*
***************************************************************************************************
*                                       EFLASHTest_RESET
*
* EFLASH RESET test
*
* @param    none
*
* @return   none
*
* Notes
*           [SM-PV] 05. Data eFLASH and Controller - DataFlash.02, Reset
*           If a system reset occurs during programming in Flash, the reset is prevented
*           until the recording is completed to ensure the integrity of the recorded data.
***************************************************************************************************
*/

void EFLASHTest_RESET
(
    uint32 uiFlag
);



/*
***************************************************************************************************
*                                       EFLASHTest_Int_Test
*
* EFLASH RESET test
*
* @param    none
*
* @return   none
*
* Notes
*           Program Flash Controller.. interrupt test
***************************************************************************************************
*/

uint32 EFLASHTest_Int_Test(void);



uint32 EFLASHTest_Int_DFLASHTest(void );

void EFLASHTest_DflashEcc(uint32 uiFlag);

void EFLASHTest_DflashAccessControl(uint32 uiFlag);
void EFLASHTest_PflashAccessControl(uint32 uiFlag);

void EFLASHTest_flashEraseWrite(uint32 type);
void EFLASHTest_snorEraseWrite(uint32 type);

#endif  // (MCU_BSP_SUPPORT_TEST_APP_EFLASH == 1 )

#endif  // MCU_BSP_EFLASH_TEST_HEADER

