/*
***************************************************************************************************
*
*   FileName : eflash_test.c
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#if ( MCU_BSP_SUPPORT_TEST_APP_EFLASH == 1 )

#include <eflash_test.h>
#include <eflash.h>
#include <bsp.h>
#include <gic.h>
#include <fmu.h>
#include <eflash.h>
#include <reg_phys.h>
#include <fmu.h>
#include <debug.h>
#include <snor_mio.h>

/*
***************************************************************************************************
*                                             DEFINITIONS
***************************************************************************************************
*/

#ifdef __GNU_C__

#define EF_SETUSER()                    __asm volatile ( "CPS 0x10" )
#define EF_SETSVC()                     __asm volatile ( "SWI 0" )
#define EFLASH_CACHE_FLUSH_TEST()       \
    asm("DSB");                         \
    asm("MCR p15, 0, r0, c7, c5, 0");   \
    asm("MCR p15, 0, r0, c15, c5, 0");  \
    asm("ISB")
#else
__asm volatile void asmEF_SETUSER()
{
    CPS 0x10;
}

__asm volatile void asmEF_SETSVC()
{
    SWI 0;
}

__asm volatile void asmEFLASH_CACHE_FLUSH_TEST()
{
    DSB;
    MCR p15, 0, r0, c7, c5, 0;
    MCR p15, 0, r0, c15, c5, 0;
    ISB;
}
#endif
#define EF_PRG_LDT0                     (0xA1011000u)
#define EF_CNT_PRG_LDT0                 (0xFFFD1000u)

#define PFLASH_PW_ADDRESS               (0xA1008060u)
#define EFLASH_PASSWORD                 (0x2939F437u)


#define PFLASH_INTEn_ADDRESS            (0xA1020000u)
#define PFLASH_INTEn_ERASE_FAIL         (   1u << 0u)
#define PFLASH_INTEn_READ_FAIL          (   1u << 1u)
#define PFLASH_INTEn_PROGADDR_FAIL      (   1u << 2u)
#define PFLASH_INTEn_PROGDATA_FAIL      (   1u << 3u)
#define PFLASH_INTEn_ECC_FAIL           (   1u << 4u)
#define PFLASH_INTEn_CMDREADY           (   1u << 5u)
#define PFLASH_INTEn_PGM_FIN            (   1u << 6u)
#define PFLASH_INTEn_RESET_FIN          (   1u << 7u)
#define PFLASH_INTEn_GLOBAL_INT         (   1u << 8u)
#define PFLASH_INTEn_WRITE_PROT         (   1u << 12u)

#define PFLASH_INTSTAT_ADDRESS          (0xA1020004u)

#define PFLASH_FAULTEn_ADDRESS          (0xA1020008u)
#define PFLASH_FAULTEn_ERASE_FAIL       (   1u << 0u)
#define PFLASH_FAULTEn_READ_FAIL        (   1u << 1u)
#define PFLASH_FAULTEn_PROGADDR_FAIL    (   1u << 2u)
#define PFLASH_FAULTEn_PROGDATA_FAIL    (   1u << 3u)
#define PFLASH_FAULTEn_ECC_FAIL         (   1u << 4u)

#define PFLASH_FAULTSTAT_ADDRESS        (0xA102000Cu)



#define DFLASH_INTEn_ADDRESS                   (0xA10A0000u)
#define DFLASH_INTEn_ECC_FAIL                  (   1u << 0u)
#define DFLASH_INTEn_CMDREADY                  (   1u << 1u)
#define DFLASH_INTEn_PGM_FIN                   (   1u << 2u)
#define DFLASH_INTEn_RESET_FIN                 (   1u << 3u)
#define DFLASH_Privilege_access_FAIL           (   1u << 4u)
#define DFLASH_Paddr_range_permission_error    (   1u << 5u)
#define DFLASH_HSM_SFR_access_permission_error (   1u << 6u)
#define DFLASH_CR5_SFR_access_permission_error (   1u << 7u)
#define DFLASH_Sema_timeout_release_error      (   1u << 8u)

#define DFLASH_INTEn_GLOBAL_INT         (   1u << 9u)
#define DFLASH_INTEn_WRITE_PROT         (   1u << 12u)

#define DFLASH_INTSTAT_ADDRESS          (0xA10A0004u)

#define DFLASH_FAULTEn_ADDRESS                         (0xA10A0008u)
#define DFLASH_FAULTEn_ECC_FAIL                        (   1u << 0u)
#define DFLASH_FAULTEn_Privilege_access_FAIL           (   1u << 1u)
#define DFLASH_FAULTEn_Paddr_range_permission_error    (   1u << 2u)
#define DFLASH_FAULTEn_HSM_SFR_access_permission_error (   1u << 3u)
#define DFLASH_FAULTEn_CR5_SFR_access_permission_error (   1u << 4u)
#define DFLASH_FAULTEn_Sema_timeout_release_error      (   1u << 5u)





#define DFLASH_FAULTSTAT_ADDRESS        (0xA10A000Cu)

/***************************************************************************************************
*                                             LOCAL VARIABLES
***************************************************************************************************/



static uint32                           gWaitIrq = 0UL;
/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/

static void EFLASHTEST_ISR
(
    void *                              pArg
);





static void EFLASHTEST_ISR(    void *pArg)
{

    uint32* puiFmuCheckArg;
    uint32  uiRegtemp = 0;

    if(pArg != NULL_PTR)
    {
        puiFmuCheckArg = (uint32 *)pArg;

        if(*puiFmuCheckArg == 1)
        {
            mcu_printf("FMU IRQ\n");
            uiRegtemp = SAL_ReadReg(PFLASH_FAULTSTAT_ADDRESS);
            if((uiRegtemp & PFLASH_FAULTEn_ERASE_FAIL) == PFLASH_FAULTEn_ERASE_FAIL)
            {
                mcu_printf("PFLASH_FAULTEn_ERASE_FAIL \n");
            }
            if((uiRegtemp & PFLASH_FAULTEn_READ_FAIL) == PFLASH_FAULTEn_READ_FAIL)
            {
                mcu_printf("PFLASH_FAULTEn_READ_FAIL \n");
            }
            if((uiRegtemp & PFLASH_FAULTEn_PROGADDR_FAIL) == PFLASH_FAULTEn_PROGADDR_FAIL)
            {
                mcu_printf("PFLASH_FAULTEn_PROGADDR_FAIL \n");
            }
            if((uiRegtemp & PFLASH_FAULTEn_PROGDATA_FAIL) == PFLASH_FAULTEn_PROGDATA_FAIL)
            {
                mcu_printf("PFLASH_FAULTEn_PROGDATA_FAIL \n");
            }
            if((uiRegtemp & PFLASH_FAULTEn_ECC_FAIL) == PFLASH_FAULTEn_ECC_FAIL)
            {
                mcu_printf("PFLASH_FAULTEn_ECC_FAIL \n");
            }
            SAL_WriteReg(uiRegtemp, PFLASH_FAULTSTAT_ADDRESS);
        }
        else
        {
            mcu_printf("GIC IRQ\n");
            uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
            if((uiRegtemp & PFLASH_INTEn_ERASE_FAIL) == PFLASH_INTEn_ERASE_FAIL)
            {
                mcu_printf("PFLASH_INTEn_ERASE_FAIL \n");
            }
            if((uiRegtemp & PFLASH_INTEn_READ_FAIL) == PFLASH_INTEn_READ_FAIL)
            {
                mcu_printf("PFLASH_INTEn_READ_FAIL \n");
            }
            if((uiRegtemp & PFLASH_INTEn_PROGADDR_FAIL) == PFLASH_INTEn_PROGADDR_FAIL)
            {
                mcu_printf("PFLASH_INTEn_PROGADDR_FAIL \n");
            }
            if((uiRegtemp & PFLASH_INTEn_PROGDATA_FAIL) == PFLASH_INTEn_PROGDATA_FAIL)
            {
                mcu_printf("PFLASH_INTEn_PROGDATA_FAIL \n");
            }
            if((uiRegtemp & PFLASH_INTEn_ECC_FAIL) == PFLASH_INTEn_ECC_FAIL)
            {
                mcu_printf("PFLASH_INTEn_ECC_FAIL \n");
            }
            if((uiRegtemp & PFLASH_INTEn_CMDREADY) == PFLASH_INTEn_CMDREADY)
            {
                mcu_printf("PFLASH_INTEn_CMDREADY \n");
            }
            if((uiRegtemp & PFLASH_INTEn_PGM_FIN) == PFLASH_INTEn_PGM_FIN)
            {
                mcu_printf("PFLASH_INTEn_PGM_FIN \n");
            }
            if((uiRegtemp & PFLASH_INTEn_RESET_FIN) == PFLASH_INTEn_RESET_FIN)
            {
                mcu_printf("PFLASH_INTEn_RESET_FIN \n");
            }
            SAL_WriteReg(uiRegtemp, PFLASH_INTSTAT_ADDRESS);
        }






#if 1//def NEVER
        if(*puiFmuCheckArg == 1)
        {
            (void)FMU_IsrClr((FMUFaultid_t)FMU_ID_PFLASH);
            //(void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH, (FMUSeverityLevelType_t)FMU_SVL_LOW, NULL_PTR, NULL_PTR);
        }
        else
        {
            (void)GIC_IntVectSet(GIC_PFLASH,
                                 GIC_PRIORITY_NO_MEAN,
                                 GIC_INT_TYPE_LEVEL_HIGH,
                                 NULL_PTR,
                                 NULL_PTR);


            (void)GIC_IntSrcDis(GIC_PFLASH);
        }
#endif /* NEVER */
        mcu_printf("please reset for another test\n");

   }


    return;
}



static void EFLASHTEST_ISR2(    void *pArg)
{
    volatile uint32  uiRegtemp = 0;
    (void)pArg;

    uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
    if((uiRegtemp & PFLASH_INTEn_ERASE_FAIL) == PFLASH_INTEn_ERASE_FAIL)
    {
        mcu_printf("PFLASH_INTEn_ERASE_FAIL \n");
    }
    if((uiRegtemp & PFLASH_INTEn_READ_FAIL) == PFLASH_INTEn_READ_FAIL)
    {
        mcu_printf("PFLASH_INTEn_READ_FAIL \n");
    }
    if((uiRegtemp & PFLASH_INTEn_PROGADDR_FAIL) == PFLASH_INTEn_PROGADDR_FAIL)
    {
        mcu_printf("PFLASH_INTEn_PROGADDR_FAIL \n");
    }
    if((uiRegtemp & PFLASH_INTEn_PROGDATA_FAIL) == PFLASH_INTEn_PROGDATA_FAIL)
    {
        mcu_printf("PFLASH_INTEn_PROGDATA_FAIL \n");
    }
    if((uiRegtemp & PFLASH_INTEn_ECC_FAIL) == PFLASH_INTEn_ECC_FAIL)
    {
        mcu_printf("PFLASH_INTEn_ECC_FAIL \n");
    }
    if((uiRegtemp & PFLASH_INTEn_CMDREADY) == PFLASH_INTEn_CMDREADY)
    {
        mcu_printf("PFLASH_INTEn_CMDREADY \n");
    }
    if((uiRegtemp & PFLASH_INTEn_PGM_FIN) == PFLASH_INTEn_PGM_FIN)
    {
        mcu_printf("PFLASH_INTEn_PGM_FIN \n");
    }
    if((uiRegtemp & PFLASH_INTEn_RESET_FIN) == PFLASH_INTEn_RESET_FIN)
    {
        mcu_printf("PFLASH_INTEn_RESET_FIN \n");
    }

    SAL_WriteReg(uiRegtemp, PFLASH_INTSTAT_ADDRESS);  /* clear int status */
    gWaitIrq = 0;



    return;
}


static void EFLASHTEST_ISR3(    void *pArg)
{
    volatile uint32  uiRegtemp = 0;
    (void)pArg;

    uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
    if((uiRegtemp & DFLASH_INTEn_ECC_FAIL) == DFLASH_INTEn_ECC_FAIL)
    {
        mcu_printf("DFLASH_INTEn_ECC_FAIL \n");
    }
    if((uiRegtemp & DFLASH_INTEn_CMDREADY) == DFLASH_INTEn_CMDREADY)
    {
        mcu_printf("DFLASH_INTEn_CMDREADY \n");
    }
    if((uiRegtemp & DFLASH_INTEn_PGM_FIN) == DFLASH_INTEn_PGM_FIN)
    {
        mcu_printf("DFLASH_INTEn_PGM_FIN \n");
    }
    if((uiRegtemp & DFLASH_INTEn_RESET_FIN) == DFLASH_INTEn_RESET_FIN)
    {
        mcu_printf("DFLASH_INTEn_RESET_FIN \n");
    }
    if((uiRegtemp & DFLASH_Privilege_access_FAIL) == DFLASH_Privilege_access_FAIL)
    {
        mcu_printf("DFLASH_Privilege_access_FAIL \n");
    }

    SAL_WriteReg(uiRegtemp, DFLASH_INTSTAT_ADDRESS);  /* clear int status */
    gWaitIrq = 0;



    return;
}

static void EFLASHTEST_ISR4(    void *pArg)
{
    uint32* puiFmuCheckArg;
    uint32  uiRegtemp = 0;

    if(pArg != NULL_PTR)
    {
       puiFmuCheckArg = (uint32 *)pArg;

        if(*puiFmuCheckArg == 1)
        {
            mcu_printf("FMU IRQ\n");
            uiRegtemp = SAL_ReadReg(DFLASH_FAULTSTAT_ADDRESS);

            if((uiRegtemp & DFLASH_FAULTEn_ECC_FAIL) == DFLASH_FAULTEn_ECC_FAIL)
            {
                mcu_printf("DFLASH_FAULTEn_ECC_FAIL check ok\n");
            }
            if((uiRegtemp & DFLASH_FAULTEn_Privilege_access_FAIL) == DFLASH_FAULTEn_Privilege_access_FAIL)
            {
                mcu_printf("DFLASH_FAULTEn_Privilege_access_FAIL check ok\n");
            }
            if((uiRegtemp & DFLASH_FAULTEn_Paddr_range_permission_error) == DFLASH_FAULTEn_Paddr_range_permission_error)
            {
                mcu_printf("DFLASH_FAULTEn_Paddr_range_permission_error check ok\n");
            }
            if((uiRegtemp & DFLASH_FAULTEn_HSM_SFR_access_permission_error) == DFLASH_FAULTEn_HSM_SFR_access_permission_error)
            {
                mcu_printf("DFLASH_FAULTEn_HSM_SFR_access_permission_error check ok\n");
            }
            if((uiRegtemp & DFLASH_FAULTEn_CR5_SFR_access_permission_error) == DFLASH_FAULTEn_CR5_SFR_access_permission_error)
            {
                mcu_printf("DFLASH_FAULTEn_CR5_SFR_access_permission_error check ok\n");
            }
            if((uiRegtemp & DFLASH_FAULTEn_Sema_timeout_release_error) == DFLASH_FAULTEn_Sema_timeout_release_error)
            {
                mcu_printf("DFLASH_FAULTEn_Sema_timeout_release_error check ok\n");
            }
            SAL_WriteReg(uiRegtemp, DFLASH_FAULTSTAT_ADDRESS);  /* clear int status */
        }
        else
        {
            mcu_printf("GIC IRQ\n");
            uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);

            if((uiRegtemp & DFLASH_INTEn_ECC_FAIL) == DFLASH_INTEn_ECC_FAIL)
            {
                mcu_printf("DFLASH_INTEn_ECC_FAIL check ok\n");
            }
            if((uiRegtemp & DFLASH_INTEn_CMDREADY) == DFLASH_INTEn_CMDREADY)
            {
                mcu_printf("DFLASH_INTEn_CMDREADY check ok\n");
            }
            if((uiRegtemp & DFLASH_INTEn_PGM_FIN) == DFLASH_INTEn_PGM_FIN)
            {
                mcu_printf("DFLASH_INTEn_PGM_FIN check ok\n");
            }
            if((uiRegtemp & DFLASH_INTEn_RESET_FIN) == DFLASH_INTEn_RESET_FIN)
            {
                mcu_printf("DFLASH_INTEn_RESET_FIN check ok\n");
            }
            if((uiRegtemp & DFLASH_Privilege_access_FAIL) == DFLASH_Privilege_access_FAIL)
            {
                mcu_printf("DFLASH_Privilege_access_FAIL check ok\n");
            }
            if((uiRegtemp & DFLASH_Paddr_range_permission_error) == DFLASH_Paddr_range_permission_error)
            {
                mcu_printf("DFLASH_Paddr_range_permission_error check ok\n");
            }

            if((uiRegtemp & DFLASH_HSM_SFR_access_permission_error) == DFLASH_HSM_SFR_access_permission_error)
            {
                mcu_printf("DFLASH_HSM_SFR_access_permission_error check ok\n");
            }
            if((uiRegtemp & DFLASH_CR5_SFR_access_permission_error) == DFLASH_CR5_SFR_access_permission_error)
            {
                mcu_printf("DFLASH_CR5_SFR_access_permission_error check ok\n");
            }
            if((uiRegtemp & DFLASH_Sema_timeout_release_error) == DFLASH_Sema_timeout_release_error)
            {
                mcu_printf("DFLASH_Sema_timeout_release_error check ok\n");
            }
            SAL_WriteReg(uiRegtemp, DFLASH_INTSTAT_ADDRESS);  /* clear int status */
        }





#if 1//def NEVER
        if(*puiFmuCheckArg == 1)
        {
//          (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH, (FMUSeverityLevelType_t)FMU_SVL_LOW, NULL_PTR, NULL_PTR);
            (void)FMU_IsrClr((FMUFaultid_t)FMU_ID_DFLASH);
        }
        else
        {
            (void)GIC_IntVectSet(GIC_DFLASH,
                                 GIC_PRIORITY_NO_MEAN,
                                 GIC_INT_TYPE_LEVEL_HIGH,
                                 NULL_PTR,
                                 NULL_PTR);


            (void)GIC_IntSrcDis(GIC_DFLASH);
        }
#endif /* NEVER */

        mcu_printf("please reset for another test\n");



   }


    return;
}

#ifdef NEVER

uint32 EFLASH_ECC_Test(uint32 uiFAULTEn)
{
    uint32 ret = FCONT_RET_OK;
    volatile uint32 reg_temp = 0;
    uint32 uitempbuff[4] = {0x12345678u, 0xFFFF1234u, 0x1234FFFFu, 0xEEEEEEEEu};
    // 1. eflash 0 address
    EFLASH_EraseMode(0u, FLS_FRWCON_PAGE_ERASE, FCONT_TYPE_PFLASH);

    // 2. set ecc off
    //it is not neccessary  SAL_WriteReg(EFLASH_PASSWORD, PFLASH_PW_ADDRESS);
    reg_temp = HwPflash_RegisterMap->TM_FSH_CON;
    reg_temp = reg_temp & 0x0FFFFFFFu;      //bit 28 ecc off
    HwPflash_RegisterMap->TM_FSH_CON = reg_temp;

    // 3. write 0x0~ 0xc
    EFLASH_WriteByte(0u, (void *)uitempbuff, 16u, FCONT_TYPE_PFLASH);

    // 4. ecc on
    reg_temp = HwPflash_RegisterMap->TM_FSH_CON;
    reg_temp = reg_temp | 0x10000000u;      //bit 28 ecc on
    HwPflash_RegisterMap->TM_FSH_CON = reg_temp;

    if(uiFAULTEn == 1)
    {
        reg_temp = SAL_ReadReg(PFLASH_FAULTEn_ADDRESS);
        reg_temp |= PFLASH_FAULTEn_ECC_FAIL;
        SAL_WriteReg(reg_temp, PFLASH_FAULTEn_ADDRESS);
    }

    reg_temp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);

    if(uiFAULTEn == 2)
    {
        reg_temp |= (PFLASH_INTEn_ECC_FAIL);
    }
    else
    {
        reg_temp |= (PFLASH_INTEn_ECC_FAIL | PFLASH_INTEn_GLOBAL_INT);
    }
    SAL_WriteReg(reg_temp, PFLASH_INTEn_ADDRESS);


    // 5. read data 0x0~0xc
    EFLASH_ReadByte(0, (void *)uitempbuff, 16u, FCONT_TYPE_PFLASH);


    return ret;
}
#endif /* NEVER */

static uint32 EFLASH_ECC_Test(uint32 uiFAULTEn)
{
    uint32 ret = FCONT_RET_OK;
    volatile uint32 reg_temp = 0;
    uint32 uitempbuff[4] = {0x12345678u, 0xFFFF1234u, 0x1234FFFFu, 0xEEEEEEEEu};
    // 1. eflash erase 0 address

    reg_temp = SAL_ReadReg(0xA1000004u);
    reg_temp = reg_temp & 0x40u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1000004u);
        reg_temp = reg_temp & 0x40u;
    }

    reg_temp = SAL_ReadReg(0xA1000004u);
    reg_temp = reg_temp & 0xF00u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1000004u);
        reg_temp = reg_temp & 0xF00u;
    }
    SAL_WriteReg(0x90000, 0xA1000200u);
    SAL_WriteReg(1u, 0xA1000000);
    reg_temp = SAL_ReadReg(0xA1000004u);
    reg_temp = reg_temp & 0xf00u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1000004u);
        reg_temp = reg_temp & 0xf00u;
    }
    reg_temp = SAL_ReadReg(0xA1000004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30)
    {
        reg_temp = SAL_ReadReg(0xA1000004u);
        reg_temp = reg_temp & 0x30u;
    }




    // 2. set ecc off
    //it is not neccessary  SAL_WriteReg(EFLASH_PASSWORD, PFLASH_PW_ADDRESS);
    reg_temp = SAL_ReadReg(0xA1000034u);
    reg_temp = reg_temp & 0x0FFFFFFFu;      //bit 28 ecc off
    SAL_WriteReg(reg_temp, 0xA1000034u);

    // 3. write 0x0~ 0xc
    SAL_WriteReg(0x90000, 0xA1000200u);
    SAL_WriteReg(uitempbuff[0], 0xA1000204u);
    SAL_WriteReg(uitempbuff[1], 0xA1000210u);
    SAL_WriteReg(uitempbuff[2], 0xA1000214u);
    SAL_WriteReg(uitempbuff[3], 0xA1000218u);
    SAL_WriteReg(2u, 0xA1000000u);

    reg_temp = SAL_ReadReg(0xA1000004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30)
    {
        reg_temp = SAL_ReadReg(0xA1000004u);
        reg_temp = reg_temp & 0x30u;
    }

#ifdef __GNU_C__
    EFLASH_CACHE_FLUSH_TEST();
#else
    asmEFLASH_CACHE_FLUSH_TEST();
#endif

    // 4. ecc on
    reg_temp = SAL_ReadReg(0xA1000034u);
    reg_temp = reg_temp | 0x10000000u;      //bit 28 ecc on
    SAL_WriteReg(reg_temp, 0xA1000034u);

    if(uiFAULTEn == 1)
    {
        reg_temp = SAL_ReadReg(PFLASH_FAULTEn_ADDRESS);
        reg_temp |= PFLASH_FAULTEn_ECC_FAIL;
        SAL_WriteReg(reg_temp, PFLASH_FAULTEn_ADDRESS);
    }

    reg_temp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);

    if(uiFAULTEn == 2)
    {
        reg_temp |= (PFLASH_INTEn_ECC_FAIL);
    }
    else
    {
        reg_temp |= (PFLASH_INTEn_ECC_FAIL | PFLASH_INTEn_GLOBAL_INT);
    }
    SAL_WriteReg(reg_temp, PFLASH_INTEn_ADDRESS);


    // 5. read data 0x0~0xc
    mcu_printf("read reg x%x\n",*(uint32 *)(0x20090000u));



    return ret;
}

/*
***************************************************************************************************
*                                       EFLASHTest_ECC
*
* EFLASH ECC test
*
* @param    none
*
* @return   none
*
* Notes
*           [SM-PV] 05. Data eFLASH and Controller - DataFlash.01, ECC (Error Correcting Code)
*           Flash memory must be able to detect 1-bit or 2-bit data errors occurring in flash
*           memory through ECC.
***************************************************************************************************
*/


void EFLASHTest_ECC(uint32 uiFlag)
{
    uint32 uiIsrFlag = 0;
    uint32 uiFmuIsrFlag = 1;
    uint32 uiRegtemp = 0;

    if(uiFlag == 0)
    {

        mcu_printf("ECC int mode test\n");

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_PFLASH);


        (void)GIC_IntVectSet(GIC_PFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_PFLASH);




        uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASH_ECC_Test(0);
    }
    else if(uiFlag == 1)
    {
        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_HIGH,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_PFLASH);

        mcu_printf("ECC int mode test\n");

        (void)GIC_IntVectSet(GIC_PFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_PFLASH);




        uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASH_ECC_Test(1u);
    }
    else if(uiFlag == 2)
    {

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_PFLASH);

        mcu_printf("ECC int mode test\n");

        (void)GIC_IntVectSet(GIC_PFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_PFLASH);




        uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASH_ECC_Test(2u);
    }
    else
    {
        mcu_printf(" int not set, ECC test\n");
        uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASH_ECC_Test(0);
        uiRegtemp = SAL_ReadReg(PFLASH_INTSTAT_ADDRESS);
        if((uiRegtemp & PFLASH_INTEn_ECC_FAIL) == PFLASH_INTEn_ECC_FAIL)
        {
            mcu_printf("PFLASH_INTEn_ECC_FAIL check ok\n");
        }
    }

    mcu_printf("=========== Done ===========\n");
    return;
}

/*
***************************************************************************************************
*                                       EFLASHTest_RESET
*
* EFLASH RESET test
*
* @param    none
*
* @return   uiFlag[in] - 1 - reset occur
*                        2 - after reset, check data recording is completed
*
* Notes
*           [SM-PV] 05. Data eFLASH and Controller - DataFlash.02, Reset
*           If a system reset occurs during programming in Flash, the reset is prevented
*           until the recording is completed to ensure the integrity of the recorded data.
***************************************************************************************************
*/


void EFLASHTest_RESET(uint32 uiFlag)
{
    const uint32 testdata = 0x12341234u;
    const uint32 pmupw    = 0x5AFEACE5u;
    volatile unsigned int   *pReadBuf = (volatile unsigned int*)(0x20000000);
    if(uiFlag == 0)
    {
        mcu_printf("=========== erase addr 0x2000 0000 ~ 0c  ===========\n");
        SAL_WriteReg(0, 0xA1000200u);
        SAL_WriteReg(1, 0xA1000000u);
        mcu_printf("=========== write addr 0x2000 0000 ~ 0c  & cold reset===========\n");

        SAL_WriteReg(0, 0xA1000200u);
        SAL_WriteReg(testdata, 0xA1000204u);
        SAL_WriteReg(testdata, 0xA1000210u);
        SAL_WriteReg(testdata, 0xA1000214u);
        SAL_WriteReg(testdata, 0xA1000218u);
        SAL_WriteReg(2u, 0xA1000000u);

        SAL_WriteReg(pmupw, 0xA0F283FC);
        SAL_WriteReg(1u, 0xA0F28010);
    }
    else
    {
        if((pReadBuf[0] == testdata) &&
           (pReadBuf[1] == testdata) &&
           (pReadBuf[2] == testdata) &&
           (pReadBuf[3] == testdata))
           {
               mcu_printf("=========== write ok ===========\n");
           }
      //read and check
    }


    return;
}



/*
***************************************************************************************************
*                                       EFLASHTest_Int_Test
*
* EFLASH RESET test
*
* @param    none
*
* @return   none
*
* Notes
*           [SM-PV] 05. Data eFLASH and Controller - DataFlash.02, Reset
*           If a system reset occurs during programming in Flash, the reset is prevented
*           until the recording is completed to ensure the integrity of the recorded data.
***************************************************************************************************
*/


uint32 EFLASHTest_Int_Test(void)
{
    uint32 ret = FCONT_RET_OK;
    volatile uint32 reg_temp = 0;
    uint32 uitempbuff[4] = {0x12345678u, 0xFFFF1234u, 0x1234FFFFu, 0xEEEEEEEEu};


    SAL_WriteReg(0x11FFu, PFLASH_INTEn_ADDRESS); //all int enable

    (void)GIC_IntVectSet(GIC_PFLASH,
                         GIC_PRIORITY_NO_MEAN,
                         GIC_INT_TYPE_LEVEL_HIGH,
                         (GICIsrFunc)&EFLASHTEST_ISR2,
                         NULL_PTR);


    (void)GIC_IntSrcEn(GIC_PFLASH);

    mcu_printf("Starts erasing the ldt0 area... expected isr is PFLASH_INTEn_ERASE_FAIL\n");
    gWaitIrq = 1;
    SAL_WriteReg(EF_CNT_PRG_LDT0, 0xA1000200u);
    SAL_WriteReg(1u, 0xA1000000);

    while(gWaitIrq == 1)
    {

    }
    mcu_printf("Starts reading the ldt0 area... expected isr is PFLASH_INTEn_READ_FAIL\n");

    gWaitIrq = 1;

    reg_temp = SAL_ReadReg(EF_PRG_LDT0);

    while(gWaitIrq == 1)
    {

    }

    mcu_printf("EF_PRG_LDT0 . readed data x5x\n",reg_temp);

    mcu_printf("Starts writing the ldt0 area... expected isr is PFLASH_INTEn_PROGADDR_FAIL\n");
    gWaitIrq = 1;

    SAL_WriteReg(EF_CNT_PRG_LDT0, 0xA1000200u);
    SAL_WriteReg(uitempbuff[0], 0xA1000204u);
    SAL_WriteReg(uitempbuff[1], 0xA1000210u);
    SAL_WriteReg(uitempbuff[2], 0xA1000214u);
    SAL_WriteReg(uitempbuff[3], 0xA1000218u);
    SAL_WriteReg(2u, 0xA1000000);

    while(gWaitIrq == 1)
    {

    }

    mcu_printf("Starts erasing address 0... expected isr is PFLASH_INTEn_CMDREADY, PFLASH_INTEn_PGM_FIN\n");
    gWaitIrq = 1;

    SAL_WriteReg(0, 0xA1000200u);
    SAL_WriteReg(1u, 0xA1000000);
    while(gWaitIrq == 1)
    {

    }

    mcu_printf("Starts writing address 0... expected isr is PFLASH_INTEn_CMDREADY, PFLASH_INTEn_PGM_FIN\n");

    gWaitIrq = 1;
    SAL_WriteReg(0, 0xA1000200u);
    SAL_WriteReg(uitempbuff[0], 0xA1000204u);
    SAL_WriteReg(uitempbuff[1], 0xA1000210u);
    SAL_WriteReg(uitempbuff[2], 0xA1000214u);
    SAL_WriteReg(uitempbuff[3], 0xA1000218u);
    SAL_WriteReg(2u, 0xA1000000);
    while(gWaitIrq == 1)
    {

    }


    mcu_printf("Starts writing invalid area(not erased)... expected isr is PFLASH_INTEn_PROGDATA_FAIL\n");

    gWaitIrq = 1;
    SAL_WriteReg(0, 0xA1000200u);
    SAL_WriteReg(uitempbuff[0], 0xA1000204u);
    SAL_WriteReg(uitempbuff[1], 0xA1000210u);
    SAL_WriteReg(uitempbuff[2], 0xA1000214u);
    SAL_WriteReg(uitempbuff[3], 0xA1000218u);
    SAL_WriteReg(2u, 0xA1000000);
    while(gWaitIrq == 1)
    {

    }


    mcu_printf("Starts reset eflash controller... expected isr is PFLASH_INTEn_RESET_FIN\n");
    gWaitIrq = 1;

    SAL_WriteReg(0x100u, 0xA1000000);
    while(gWaitIrq == 1)
    {

    }
    mcu_printf("---eflash int test complete......\n");
    return ret;
}


static uint32 EFLASHTest_AccCnt(uint32 uiFAULTEn)
{
    uint32 ret = FCONT_RET_OK;
    volatile uint32 reg_temp = 0;
    uint32 ret_cnt = 3;

    if(uiFAULTEn == 1)
    {
        reg_temp = SAL_ReadReg(PFLASH_FAULTEn_ADDRESS);
        reg_temp |= (PFLASH_FAULTEn_READ_FAIL | PFLASH_FAULTEn_PROGADDR_FAIL);
        SAL_WriteReg(reg_temp, PFLASH_FAULTEn_ADDRESS);
    }

    reg_temp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);



    reg_temp |= 0x1FFu; //all enable
    SAL_WriteReg(reg_temp, PFLASH_INTEn_ADDRESS);

#ifdef __GNU_C__
    EF_SETUSER();
#else
    asmEF_SETUSER();
#endif

    // 5. read data 0x0~0xc
    if(uiFAULTEn == 2)
    {
        mcu_printf("attempt read 0x20000000 : x%x, SAL_ReadReg(0xA1000004u) status : x%x\n",*(uint32 *)(0x20000000u), SAL_ReadReg(0xA1000004u));
    }

    SAL_WriteReg(0, 0xA1000200u);
    SAL_WriteReg(0x12345678u, 0xA1000204u);
    SAL_WriteReg(0x12345678u, 0xA1000210u);
    SAL_WriteReg(0x12345678u, 0xA1000214u);
    SAL_WriteReg(0x12345678u, 0xA1000218u);
    SAL_WriteReg(2u, 0xA1000000);
    reg_temp = SAL_ReadReg(0xA1000004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30 && ret_cnt > 0)
    {
        reg_temp = SAL_ReadReg(0xA1000004u);
        reg_temp = reg_temp & 0x30u;
//      ret_cnt--;
    }

#ifdef __GNU_C__
        EFLASH_CACHE_FLUSH_TEST();
#else
        asmEFLASH_CACHE_FLUSH_TEST();
#endif

    return ret;
}


void EFLASHTest_PflashAccessControl(uint32 uiFlag)
{
    uint32 uiIsrFlag =  0;
    uint32 uiFmuIsrFlag = 1;
    uint32 uiRegtemp = 0;

    if(uiFlag == 0)
    {

        mcu_printf("ACC cnt int mode test\n");

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_PFLASH);


        (void)GIC_IntVectSet(GIC_PFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_PFLASH);




        uiRegtemp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }


        EFLASHTest_AccCnt(0);

    }
    else if(uiFlag == 1)
    {
        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_PFLASH);

        mcu_printf("ACC cnt  int mode test\n");

        (void)GIC_IntVectSet(GIC_PFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_PFLASH);




        uiRegtemp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_AccCnt(1u);
    }
    else if(uiFlag == 2)
    {

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_PFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_PFLASH);

        mcu_printf("ACC cnt  int mode test\n");

        (void)GIC_IntVectSet(GIC_PFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_PFLASH);




        uiRegtemp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_AccCnt(2u);
    }
    else
    {
        mcu_printf(" ACC cnt int not set, ECC test\n");
        uiRegtemp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_AccCnt(0);
        uiRegtemp = SAL_ReadReg(PFLASH_INTEn_ADDRESS);
        if((uiRegtemp & DFLASH_INTEn_ECC_FAIL) == DFLASH_INTEn_ECC_FAIL)
        {
            mcu_printf("DFLASH_INTEn_ECC_FAIL check ok\n");
        }
    }

    mcu_printf("=========== Done ===========\n");
    return;
}

/////////////////////////////////////////////////////DFLASH
uint32 EFLASHTest_Int_DFLASHTest(void)
{
    uint32 ret = FCONT_RET_OK;

    uint32 uitempbuff[4] = {0x12345678u, 0xFFFF1234u, 0x1234FFFFu, 0xEEEEEEEEu};

    uint32 LockNSel = *(volatile uint32 *)(0xA10A0014U);//FLS_HARDWARE_E_DFLASH_LOCK_SEL_ADDRESS);
    uint32 targetAddr = 0u;
    uint8 Lock = (LockNSel & 0x40u) >> 6u;
    uint8 Sel  = (LockNSel & 0x30u) >> 4u;
//    uint32 intFlag = (DFLASH_INTEn_GLOBAL_INT | DFLASH_INTEn_PGM_FIN)
    if(Lock == 1u)
    {
        targetAddr += (Sel * 0x10000u);
    }
    else
    {
        mcu_printf("dflash belonging to r5 does not exist\n");
    }


    SAL_WriteReg(0x3FFu, DFLASH_INTEn_ADDRESS); //all int enable


    (void)GIC_IntVectSet(GIC_DFLASH,
                         GIC_PRIORITY_NO_MEAN,
                         GIC_INT_TYPE_LEVEL_HIGH,
                         (GICIsrFunc)&EFLASHTEST_ISR3,
                         NULL_PTR);


    (void)GIC_IntSrcEn(GIC_DFLASH);




    mcu_printf("Starts erasing address 0... expected isr is DFLASH_INTEn_CMDREADY, DFLASH_INTEn_PGM_FIN\n");
    gWaitIrq = 1;
    //0. hw sema set

    SAL_WriteReg(1u, 0xA10A001CU);

    SAL_WriteReg(targetAddr, 0xA1080200u);
    SAL_WriteReg(1u, 0xA1080000);
    while(gWaitIrq == 1)
    {

    }

    mcu_printf("Starts writing address 0... expected isr is DFLASH_INTEn_CMDREADY, DFLASH_INTEn_PGM_FIN\n");

    gWaitIrq = 1;
    SAL_WriteReg(targetAddr, 0xA1080200u);
    SAL_WriteReg(uitempbuff[0], 0xA1080204u);
    SAL_WriteReg(uitempbuff[1], 0xA1080210u);
    SAL_WriteReg(2u, 0xA1080000);
    while(gWaitIrq == 1)
    {

    }




    mcu_printf("Starts reset eflash controller... expected isr is PFLASH_INTEn_RESET_FIN\n");
    gWaitIrq = 1;

    SAL_WriteReg(0x100u, 0xA1080000);
    while(gWaitIrq == 1)
    {

    }
    SAL_WriteReg(0u, 0xA10A001CU);
    mcu_printf("---eflash int test complete......\n");
    return ret;
}

static uint32 EFLASHTest_ECC_DFlash(uint32 uiFAULTEn)
{
    uint32 ret = FCONT_RET_OK;
    volatile uint32 reg_temp = 0;
    uint32 uitempbuff[2] = {0x12345678u, 0xFFFF1234u};
    uint32 LockNSel = *(volatile uint32 *)(0xA10A0014U);//FLS_HARDWARE_E_DFLASH_LOCK_SEL_ADDRESS);
    uint32 targetAddr = 0u;
    uint8 Lock = (LockNSel & 0x40u) >> 6u;
    uint8 Sel  = (LockNSel & 0x30u) >> 4u;

    if(Lock == 1u)
    {
        targetAddr += (Sel * 0x10000u);
    }
    else
    {
        mcu_printf("dflash belonging to r5 does not exist\n");
    }

    //0. hw sema set

    SAL_WriteReg(1u, 0xA10A001CU);


    // 1. eflash targetAddr address


    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0x40u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0x40u;
    }
    SAL_WriteReg(targetAddr, 0xA1080200u);
    SAL_WriteReg(1u, 0xA1080000);
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0xf00u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0xf00u;
    }
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0x30u;
    }


    // 2. set ecc off
    //it is not neccessary  SAL_WriteReg(EFLASH_PASSWORD, PFLASH_PW_ADDRESS);

    reg_temp = SAL_ReadReg(0xA1080034u);
    reg_temp = reg_temp & 0x0FFFFFFFu;      //bit 28 ecc off
    SAL_WriteReg(reg_temp, 0xA1080034u);

    // 3. write 0x0~ 0x4
    SAL_WriteReg(targetAddr, 0xA1080200u);
    SAL_WriteReg(uitempbuff[0], 0xA1080204u);
    SAL_WriteReg(uitempbuff[1], 0xA1080210u);
    SAL_WriteReg(2u, 0xA1080000);
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0x30u;
    }
#ifdef __GNU_C__
        EFLASH_CACHE_FLUSH_TEST();
#else
        asmEFLASH_CACHE_FLUSH_TEST();
#endif


    // 4. ecc on
    reg_temp = SAL_ReadReg(0xA1080034u);
    reg_temp = reg_temp | 0x10000000u;      //bit 28 ecc on
    SAL_WriteReg(reg_temp, 0xA1080034u);

    if(uiFAULTEn == 1)
    {
        reg_temp = SAL_ReadReg(DFLASH_FAULTEn_ADDRESS);
        reg_temp |= (DFLASH_FAULTEn_ECC_FAIL | DFLASH_FAULTEn_Privilege_access_FAIL);
        SAL_WriteReg(reg_temp, DFLASH_FAULTEn_ADDRESS);
    }

    reg_temp = SAL_ReadReg(DFLASH_INTEn_ADDRESS);

    reg_temp |= (DFLASH_INTEn_ECC_FAIL | DFLASH_Privilege_access_FAIL | DFLASH_INTEn_GLOBAL_INT);

    SAL_WriteReg(reg_temp, DFLASH_INTEn_ADDRESS);


    // 5. read data 0x0~0xc

    mcu_printf("read reg x%x\n",*(uint32 *)(0x30000000u+targetAddr));
    SAL_WriteReg(0u, 0xA10A001CU);
    return ret;
}


static uint32 EFLASHTest_DAccCnt(uint32 uiFAULTEn)
{
    uint32 ret = FCONT_RET_OK;
    volatile uint32 reg_temp = 0;
    uint32 ret_cnt = 3;
    uint32 LockNSel = *(volatile uint32 *)(0xA10A0014U);//FLS_HARDWARE_E_DFLASH_LOCK_SEL_ADDRESS);
    uint32 targetAddr = 0u;
    uint8 Lock = (LockNSel & 0x40u) >> 6u;
    uint8 Sel  = (LockNSel & 0x30u) >> 4u;

    if(Lock == 1u)
    {
        targetAddr += (Sel * 0x10000u);
    }
    else
    {
        mcu_printf("dflash belonging to r5 does not exist\n");
    }
    //0. hw sema set

    SAL_WriteReg(1u, 0xA10A001CU);

//clear test area
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0x40u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0x40u;
    }
    SAL_WriteReg(targetAddr, 0xA1080200u);
    SAL_WriteReg(1u, 0xA1080000);
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0xf00u;
    while(reg_temp != 0)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0xf00u;
    }
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0x30u;
    }


///


    if(uiFAULTEn == 1)
    {
        reg_temp = SAL_ReadReg(DFLASH_FAULTEn_ADDRESS);
        reg_temp |= (DFLASH_FAULTEn_ECC_FAIL | DFLASH_FAULTEn_Privilege_access_FAIL);
        SAL_WriteReg(reg_temp, DFLASH_FAULTEn_ADDRESS);
    }

    reg_temp = SAL_ReadReg(DFLASH_INTEn_ADDRESS);

    reg_temp |= (DFLASH_INTEn_ECC_FAIL | DFLASH_Privilege_access_FAIL | DFLASH_INTEn_GLOBAL_INT);;

    SAL_WriteReg(reg_temp, DFLASH_INTEn_ADDRESS);

#ifdef __GNU_C__
    EF_SETUSER();
#else
    asmEF_SETUSER();
#endif


    // 5. read data 0x0~0xc

    if(uiFAULTEn == 2)
    {

        mcu_printf("read reg x%x\n",*(uint32 *)(0x30000000u+targetAddr));
    }

    SAL_WriteReg(targetAddr, 0xA1080200u);
    SAL_WriteReg(0x12345678u, 0xA1080204u);
    SAL_WriteReg(0x12345678u, 0xA1080210u);
    SAL_WriteReg(2u, 0xA1080000);
    reg_temp = SAL_ReadReg(0xA1080004u);
    reg_temp = reg_temp & 0x30u;
    while(reg_temp != 0x30 && ret_cnt > 0)
    {
        reg_temp = SAL_ReadReg(0xA1080004u);
        reg_temp = reg_temp & 0x30u;
        ret_cnt--;
    }

#ifdef __GNU_C__
    EF_SETSVC();
#else
    asmEF_SETSVC();
#endif

#ifdef NEVER
#ifdef __GNU_C__
        EFLASH_CACHE_FLUSH_TEST();
#else
        asmEFLASH_CACHE_FLUSH_TEST();
#endif
#endif /* NEVER */
    SAL_WriteReg(0u, 0xA10A001CU);

    return ret;
}


void EFLASHTest_DflashEcc(uint32 uiFlag)
{
    uint32 uiIsrFlag=  0;
    uint32 uiFmuIsrFlag =  1;
    uint32 uiRegtemp = 0;

    if(uiFlag == 0)
    {

        mcu_printf("ECC int mode test\n");

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR4,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_DFLASH);


        (void)GIC_IntVectSet(GIC_DFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR4,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_DFLASH);




        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }



        EFLASHTest_ECC_DFlash(0);

    }
    else if(uiFlag == 1)
    {
        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_HIGH,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR4,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_DFLASH);

        mcu_printf("ECC int mode test\n");

        (void)GIC_IntVectSet(GIC_DFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR4,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_DFLASH);




        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_ECC_DFlash(1u);
    }
    else if(uiFlag == 2)
    {

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR4,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_DFLASH);

        mcu_printf("ECC int mode test\n");

        (void)GIC_IntVectSet(GIC_DFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR4,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_DFLASH);




        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_ECC_DFlash(2u);
    }
    else
    {
        mcu_printf(" int not set, ECC test\n");
        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_ECC_DFlash(0);
        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if((uiRegtemp & DFLASH_INTEn_ECC_FAIL) == DFLASH_INTEn_ECC_FAIL)
        {
            mcu_printf("DFLASH_INTEn_ECC_FAIL check ok\n");
        }
    }

    mcu_printf("=========== Done ===========\n");
    return;
}

void EFLASHTest_DflashAccessControl(uint32 uiFlag)
{
    uint32 uiIsrFlag=  0;
    uint32 uiFmuIsrFlag =  1;

    uint32 uiRegtemp = 0;


    if(uiFlag == 0)
    {

        mcu_printf("ACC cnt int mode test\n");

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR4,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_DFLASH);


        (void)GIC_IntVectSet(GIC_DFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR4,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_DFLASH);




        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }


        EFLASHTest_DAccCnt(0);

    }
    else if(uiFlag == 1)
    {
        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR4,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_DFLASH);

        mcu_printf("ACC cnt  int mode test\n");

        (void)GIC_IntVectSet(GIC_DFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR4,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_DFLASH);




        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_DAccCnt(1u);
    }
    else if(uiFlag == 2)
    {

        (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_DFLASH,
                            (FMUSeverityLevelType_t)FMU_SVL_LOW,
                            (FMUIntFnctPtr)&EFLASHTEST_ISR4,
                            (void *)&uiFmuIsrFlag);

        (void)FMU_Set((FMUFaultid_t)FMU_ID_DFLASH);

        mcu_printf("ACC cnt  int mode test\n");

        (void)GIC_IntVectSet(GIC_DFLASH,
                             GIC_PRIORITY_NO_MEAN,
                             GIC_INT_TYPE_LEVEL_HIGH,
                             (GICIsrFunc)&EFLASHTEST_ISR4,
                             &uiIsrFlag);


        (void)GIC_IntSrcEn(GIC_DFLASH);




        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_DAccCnt(2u);
    }
    else
    {
        mcu_printf(" ACC cnt int not set, ECC test\n");
        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if(uiRegtemp != 0)
        {
            mcu_printf("Eflash Interrupt status register is not 0, It is not in a normal operating state. \n");
            mcu_printf("stop ecc test \n");
        }

        EFLASHTest_DAccCnt(0);
        uiRegtemp = SAL_ReadReg(DFLASH_INTSTAT_ADDRESS);
        if((uiRegtemp & DFLASH_INTEn_ECC_FAIL) == DFLASH_INTEn_ECC_FAIL)
        {
            mcu_printf("DFLASH_INTEn_ECC_FAIL check ok\n");
        }
    }

    mcu_printf("=========== Done ===========\n");
    return;
}

static uint8 bufftemp[2048];

void EFLASHTest_flashEraseWrite(uint32 type)
{

    uint32 uI;
    uint32 targetAddr   = 0u;
    uint32 targetSize   = 0x10000u;//64k//256k
    uint32 retTick      = 0u;
    uint32 retEraseTick = 0;
    uint32 retWriteTick = 0;
    uint32 retCmpTick = 0;
    sint32 sRet            = 0;   /* to get the return of SAL_MemCmp */
    const uint32 DflashOffset = 0x30000u;  //R5 accessabile
    if(type == FCONT_TYPE_PFLASH)
    {
        targetSize = 0x200000u;//2M
    }
    for(uI = 0u; uI <2048u; uI++)\
    {
        bufftemp[uI] = uI % 256u;
    }
    (void)SAL_GetTickCount(&retTick);

    //ease

    if(type == FCONT_TYPE_PFLASH)
    {
        EFLASH_Erase(0u,targetSize,FCONT_TYPE_PFLASH);
    }
    else
    {
        EFLASH_Erase(DflashOffset,targetSize,FCONT_TYPE_DFLASH);
    }
    (void)SAL_GetTickCount(&retEraseTick);

    //write

    if(type == FCONT_TYPE_PFLASH)
    {
        for(targetAddr = 0u; targetAddr < targetSize; targetAddr+= 2048u)
        {
            EFLASH_WriteByte(targetAddr, bufftemp, 2048u,FCONT_TYPE_PFLASH);
        }
    }
    else
    {
        for(targetAddr = DflashOffset; targetAddr < (DflashOffset + targetSize); targetAddr+= 2048u)
        {
            EFLASH_WriteByte(targetAddr, bufftemp, 2048u,FCONT_TYPE_DFLASH);
        }
    }
    (void)SAL_GetTickCount(&retWriteTick);

    if(type == FCONT_TYPE_PFLASH)
    {
        for(targetAddr = 0u; targetAddr < targetSize; targetAddr+= 2048u)
        {
            SAL_MemCmp((const void *)bufftemp, (const void *)(targetAddr+0x20000000), 2048u, &sRet);
            if (sRet != 0)
            {
                mcu_printf("targetAddr x%x cmpare fail \n",targetAddr);
            }
        }
    }
    else
    {
        for(targetAddr = DflashOffset; targetAddr < (DflashOffset + targetSize); targetAddr+= 2048u)
        {
            SAL_MemCmp((const void *)bufftemp, (const void *)(targetAddr+0x30000000), 2048u, &sRet);
            if (sRet != 0)
            {
                mcu_printf("targetAddr x%x cmpare fail \n",targetAddr);
            }
        }
    }

    (void)SAL_GetTickCount(&retCmpTick);

    mcu_printf("type x%x \n",type);
    mcu_printf("tick erase %d\n", retEraseTick - retTick);
    mcu_printf("tick write %d\n",retWriteTick- retEraseTick);
    mcu_printf("tick cmp %d\n",retCmpTick- retWriteTick);

    return;
}

void EFLASHTest_snorEraseWrite(uint32 type)
{

    uint32 uI;
    uint32 targetAddr   = 0u;
    uint32 targetSize   = 0x200000u;
    uint32 retTick      = 0u;
    uint32 retEraseTick = 0u;
    uint32 retWriteTick = 0u;
    uint32 retCmpTick   = 0u;
    sint32 sRet         = 0;   /* to get the return of SAL_MemCmp */
    uint32 ret          = 0u;
    uint32 uiDrvidx     = SFMC0;
    uint32 uiReadOffset = 0u;
    if(type == 1)
    {
        uiDrvidx = SFMC1;
        uiReadOffset = 0x48000000u;
    }
    else
    {
        *(volatile uint32 *)(0xA0F26048) = 0x5afeace5;
        *(volatile uint32 *)(0xA0F26154) = 0;
        uiReadOffset = 0x40000000u;
    }
    if(uiDrvidx == SFMC0)
    {
        *(volatile uint32 *)(0xA0F24028) = 0xE0000005; //100Mhz //for test
    }
    else
    {
        *(volatile uint32 *)(0xA0F24060) = 0xE0000005; //100Mhz //for test
    }

    ret = SNOR_MIO_Init(uiDrvidx);
    if (ret == 0)
    {
        mcu_printf("SFMC%d Init Done.\n",uiDrvidx);
    }


    for(uI = 0u; uI <2048u; uI++)\
    {
        bufftemp[uI] = uI % 256u;
    }


    (void)SAL_GetTickCount(&retTick);

    //ease

    SNOR_MIO_Erase(uiDrvidx, 0u,targetSize);

    (void)SAL_GetTickCount(&retEraseTick);
    mcu_printf("tick erase %d\n", retEraseTick - retTick);
    (void)SAL_GetTickCount(&retEraseTick);

    //write


    for(targetAddr = 0u; targetAddr < targetSize; targetAddr+= 2048u)
    {
        SNOR_MIO_Write(uiDrvidx, targetAddr, bufftemp, 2048u);
    }

    (void)SAL_GetTickCount(&retWriteTick);
    mcu_printf("tick write %d\n",retWriteTick- retEraseTick);


    (void)SAL_GetTickCount(&retWriteTick);
    SNOR_MIO_AutoRun(uiDrvidx, 0);


    for(targetAddr = 0u; targetAddr < targetSize; targetAddr+= 2048u)
    {
        SAL_MemCmp((const void *)bufftemp, (const void *)(targetAddr+uiReadOffset), 2048u, &sRet);
        if (sRet != 0)
        {
            mcu_printf("targetAddr x%x cmpare fail \n",targetAddr);
        }
    }


    (void)SAL_GetTickCount(&retCmpTick);

    mcu_printf("type x%x \n",type);
//    mcu_printf("tick erase %d\n", retEraseTick - retTick);
//    mcu_printf("tick write %d\n",retWriteTick- retEraseTick);
    mcu_printf("tick cmp %d\n",retCmpTick- retWriteTick);
   // SNOR_MIO_AutoRun(SFMC0,1);

    return;
}

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_EFLASH == 1 )

