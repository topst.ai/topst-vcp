/*
***************************************************************************************************
*
*   FileName : pdm_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_PDM_TEST_HEADER
#define MCU_BSP_PDM_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_PDM == 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_PDM != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_PDM value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_PDM != 1 )

/**************************************************************************************************
*                                            DEFINITIONS
**************************************************************************************************/

#define PDM_CH_0                        (0UL)
#define PDM_OUT_SEL_CH                  (GPIO_PERICH_CH0) /* 0:GPIO-A, 1:GPIO-B, 2:GPIO-C, 3:GPIO-K */
#define PDM_CH3_FS                      (PMIO_GPK(8)|PMIO_GPK(9)|PMIO_GPK(10)|PMIO_GPK(11)|PMIO_GPK(12)|PMIO_GPK(13)|PMIO_GPK(14)|PMIO_GPK(15)|PMIO_GPK(16)|PMIO_GPK(17))

#define LED_PWR_EN                      (GPIO_GPC(11UL))


/*
*********************************************************************************************************
*                                                 EXTERNS
*********************************************************************************************************
*/


/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/

/*
***************************************************************************************************
*                                           PDM_SelectTestCase
*
* @param test case number
* @return
*
* Notes
*
***************************************************************************************************
*/

void PDM_SelectTestCase
(
    uint32                              uiTestCase
);

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_PDM == 1 )

#endif  // MCU_BSP_PDM_TEST_HEADER

