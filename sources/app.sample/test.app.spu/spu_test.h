/*
***************************************************************************************************
*
*   FileName : spu_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_SPU_TEST_HEADER
#define MCU_BSP_SPU_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_SPU == 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_I2S != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_I2S value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_I2S != 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_SPU != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_SPU value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_SPU != 1 )

#if ( MCU_BSP_SUPPORT_TEST_APP_AUDIO != 1 )
    #error MCU_BSP_SUPPORT_TEST_APP_AUDIO value must be 1.
#endif  // ( MCU_BSP_SUPPORT_TEST_APP_AUDIO != 1 )

/*
***************************************************************************************************
*                                           SPU_SelectTestCase
*
* @param test case number
* @return
*
* Notes
*
***************************************************************************************************
*/
void SPU_SelectTestCase
(
    uint8 ucArgc,
    void* pArgv[]
);

/*
***************************************************************************************************
*                                           SPU_Test_SSL_With_Key
*
* @param
* @return
*
* Notes
*
***************************************************************************************************
*/
void SPU_Test_SSL_With_Key
(
    unsigned int                        key_num
);

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_SPU == 1 )

#endif  // MCU_BSP_SPU_TEST_HEADER

