/*
***************************************************************************************************
*
*   FileName : lin_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_LIN_TEST_HEADER
#define MCU_BSP_LIN_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_LIN == 1)

#if ( MCU_BSP_SUPPORT_DRIVER_UART != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_UART value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_UART != 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_LIN != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_LIN value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_LIN != 1 )

/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/


void LIN_TestUsage
(
    void
);

void LIN_SampleTest
(
    uint8                               ucArgc,
    void *                              pArgv[]
);

void LIN_CreateAppTask
(
    void*                               pArg
);

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_LIN == 1 )

#endif  // MCU_BSP_LIN_TEST_HEADER

