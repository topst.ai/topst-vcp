/*
***************************************************************************************************
*
*   FileName : dse_test.c
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#if ( MCU_BSP_SUPPORT_TEST_APP_DSE == 1 )

#include <dse_test.h>
#include <bsp.h>
#include <dse.h>
#include <reg_phys.h>
#include <fmu.h>
#include <debug.h>

/*
***************************************************************************************************
*                                             DEFINITIONS
***************************************************************************************************
*/
#define DSETEST_SM_PW                   (0x5AFEACE5UL)
#define DSETEST_SM_SF_CTRL_PW           (MCU_BSP_SYSSM_BASE + 0x1C0UL)
#define DSETEST_SM_SF_CTRL_CFG          (MCU_BSP_SYSSM_BASE + 0x1F0UL)
#define DSETEST_SM_SF_CTRL_STS          (MCU_BSP_SYSSM_BASE + 0x1F4UL)

#define DSETEST_DSE_IRQ_MASK            (MCU_BSP_DSE_BASE + 0x04UL)
#define DSETEST_DSE_IRQ_EN              (MCU_BSP_DSE_BASE + 0x08UL)
#define DSETEST_DEF_SLV_CFG             (MCU_BSP_DSE_BASE + 0x0CUL)



/***************************************************************************************************
*                                             LOCAL VARIABLES
***************************************************************************************************/



static uint32                           gWaitIrq = 0UL;
/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/

static void DSE_TEST_Fmu_IRQ_ISR
(
    void *                              pArg
);


/*
***************************************************************************************************
*                                       DSE_Test_R5AccessFault
*
* Default Slave Error Sample test code
*
* @param    none
*
* @return   none
*
* Notes     Access the inaccessible address and check the dse error interrupt.
*
***************************************************************************************************
*/

void DSE_Test_R5AccessFault(void)
{
    uint32       uiI = 0;

    uint32       uiTargetAddr;
    uint32       uiTestEndAddr;

    const uint32 uiDelaycnt   = (50UL);

    DSE_Deinit();
    (void)DSE_Init(DES_SEL_ALL);

    mcu_printf("=========== DSE_Test_R5AccessFault ===========\n");

    uiTargetAddr  = 0xA0F00000u;
    uiTestEndAddr = 0xA0F2AB00u;

    for(uiTargetAddr  = 0xA0F00000u; uiTargetAddr < uiTestEndAddr; uiTargetAddr = (uiTargetAddr + 200u))
    {
        mcu_printf("Try to access @0x%x\n", uiTargetAddr);
        gWait_irq = 1UL;
        SAL_ReadReg(uiTargetAddr);


        for (uiI = 0 ; (uiI < uiDelaycnt) ; uiI++)
        {
            if(gWait_irq == 0UL)
            {
                break;
            }

            BSP_NOP_DELAY();
        }

        if(uiI == uiDelaycnt)
        {
            mcu_printf("Interrupt not occured\n");
        }

    }


    mcu_printf("=========== Done ===========\n");
    DSE_Deinit();
    return;
}


/*
***************************************************************************************************
*                                       DSE_TEST_Fmu_IRQ_ISR
*
* DSE Soft fault check , fmu callback isr function
*
* @param    pArg : uiFmuArg at the DSE_Test_SoftFaultCheck
*
* @return   none
*
* Notes
*
***************************************************************************************************
*/

static void DSE_TEST_Fmu_IRQ_ISR(void *pArg)
{
    uint32 uiRdata;
    uint32 uiReadSts;
    uint32 *uiTargetAddr;
    uint32 *uiTargetValue;

    uiRdata          = 0UL;
    uiReadSts        = 0UL;
    uiTargetAddr     = (uint32 *)0;
    uiTargetValue    = (uint32 *)0;

    if((pArg != NULL_PTR) && (*(uint32*)(pArg + 4) != (uint32)NULL))
    {
        uiTargetValue = (uint32*)pArg;
        uiTargetAddr  = (uint32*)(pArg + 4);

        uiReadSts = SAL_ReadReg(DSETEST_SM_SF_CTRL_STS);
        mcu_printf("FMU interrupt ... SF_CTRL_STS x%x\n", uiReadSts);

        if(uiReadSts == 0x4UL)
        {
            uiRdata = DSE_GetGrpSts();

            if((uiRdata & DSE_SM_IRQ_MASK) == DSE_SM_IRQ_MASK)
            {
                mcu_printf("DSE_IRQ_MASK error\n");
            }

            if((uiRdata & DSE_SM_IRQ_EN) == DSE_SM_IRQ_EN)
            {
                mcu_printf("DSE_SM_IRQ_EN error\n");
            }

            if((uiRdata & DSE_SM_SLV_CFG) == DSE_SM_SLV_CFG)
            {
                mcu_printf("DSE_SM_SLV_CFG error\n");
            }

            if((uiRdata & DSE_SM_CFG_WR_PW) == DSE_SM_CFG_WR_PW)
            {
                mcu_printf("DSE_SM_CFG_WR_PW error\n");
            }

            if((uiRdata & DSE_SM_CFG_WR_LOCK) == DSE_SM_CFG_WR_LOCK)
            {
                mcu_printf("DSE_SM_CFG_WR_LOCK error\n");
            }

        }

        SAL_WriteReg(DSETEST_SM_PW, DSETEST_SM_SF_CTRL_PW);
        uiRdata = SAL_ReadReg(DSETEST_SM_SF_CTRL_PW);

        if(uiRdata == 1UL)
        {
            mcu_printf("restore previous reg x%x, val x%x.. \n", *uiTargetAddr, *uiTargetValue);
            SAL_WriteReg(*uiTargetValue, *uiTargetAddr);
            SAL_WriteReg(uiReadSts, DSETEST_SM_SF_CTRL_STS);
            uiReadSts = SAL_ReadReg(DSETEST_SM_SF_CTRL_STS);

            if(uiReadSts != 0UL)
            {
                mcu_printf("Fail.. DSETEST_SM_SF_CTRL_STS not 0..maybe..exist another Fault\n");
            }

        }
        else
        {
            mcu_printf("Fail.. SM_SF_CTRL_PW set\n");
        }

        (void)FMU_IsrClr((FMUFaultid_t)FMU_ID_SYS_SM_CFG);

        if(gWaitIrq > 0)
        {
            gWaitIrq--;
        }
    }

    return;
}




/*
***************************************************************************************************
*                                       DSE_Test_SoftFaultCheck
*
* DSE Soft fault check test code
*
* @param    none
*
* @return   none
*
* Notes
*
***************************************************************************************************
*/

void DSE_Test_SoftFaultCheck(void)
{
#if defined( MCU_BSP_SUPPORT_DRIVER_FMU ) && ( MCU_BSP_SUPPORT_DRIVER_FMU == 1 )
    uint32 uiRdata;
    uint32 uiErr;
    uint32 uiFmuArg[2];
    uint32 uiI;

    uiRdata     = 0UL;
    uiErr       = 0UL;
    uiI         = 0UL;
    uiFmuArg[0] = 0UL;
    uiFmuArg[1] = 0UL;

    mcu_printf("\n Fault Injection Test \n");
    (void)DSE_Init(DES_SEL_ALL);
    (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_SYS_SM_CFG, (FMUSeverityLevelType_t)FMU_SVL_LOW, (FMUIntFnctPtr)&DSE_TEST_Fmu_IRQ_ISR, (void *)uiFmuArg);
    (void)FMU_Set((FMUFaultid_t)FMU_ID_SYS_SM_CFG);
    (void)FMU_IsrClr((FMUFaultid_t)FMU_ID_SYS_SM_CFG);

    SAL_WriteReg(DSETEST_SM_PW, DSETEST_SM_SF_CTRL_PW);
    uiRdata = SAL_ReadReg(DSETEST_SM_SF_CTRL_PW);

    if(uiErr == 0UL)
    {

        if(DSE_WriteLock(0UL) == SAL_RET_SUCCESS)
        {

            if(uiRdata != 1UL)
            {
                mcu_printf("  Fail.. DSETEST_SM_SF_CTRL_PW set\n");
                uiErr |= (0x10UL);
            }
            else
            {
                uiRdata = SAL_ReadReg(DSETEST_SM_SF_CTRL_CFG);
                /* for write timeout to 0xfff and dse fault en, test mod */
                SAL_WriteReg(uiRdata | 0xFFF0044UL, DSETEST_SM_SF_CTRL_CFG);
                mcu_printf("  SF_CTRL_CFG set\n");
            }

            gWaitIrq = 1UL;
            mcu_printf("  Fault injection>> DSE_IRQ_MASK\n");
            uiFmuArg[0] = SAL_ReadReg(DSETEST_DSE_IRQ_MASK);
            uiFmuArg[1] = DSETEST_DSE_IRQ_MASK;
            SAL_WriteReg( ~uiFmuArg[0], DSETEST_DSE_IRQ_MASK);

            gWaitIrq++;
            mcu_printf("  Fault injection>> DSE_IRQ_EN\n");
            uiFmuArg[0] = SAL_ReadReg(DSETEST_DSE_IRQ_EN);
            uiFmuArg[1] = DSETEST_DSE_IRQ_EN;
            SAL_WriteReg( ~uiFmuArg[0], DSETEST_DSE_IRQ_EN);

            gWaitIrq++;
            mcu_printf("  Fault injection>> DEF_SLV_CFG\n");
            uiFmuArg[0] = SAL_ReadReg(DSETEST_DEF_SLV_CFG);
            uiFmuArg[1] = DSETEST_DEF_SLV_CFG;
            SAL_WriteReg( ~uiFmuArg[0], DSETEST_DEF_SLV_CFG);


            for(uiI = 0 ; uiI < 50000UL ; uiI++)
            {
                BSP_NOP_DELAY();
                if(gWaitIrq == 0UL)
                {
                    break;
                }
            }
        }


        SAL_WriteReg(DSETEST_SM_PW, DSETEST_SM_SF_CTRL_PW);
        uiRdata = SAL_ReadReg(DSETEST_SM_SF_CTRL_PW);

        if(uiRdata != 1UL)
        {
            mcu_printf("  Fail.. DSETEST_SM_SF_CTRL_PW set\n");
            uiErr |= (0x40UL);
        }
        else
        {
            uiRdata = SAL_ReadReg(DSETEST_SM_SF_CTRL_CFG);
            SAL_WriteReg(uiRdata & (~0x44UL), DSETEST_SM_SF_CTRL_CFG);
        }

    }

    mcu_printf("  Fault Injection Test Done uiErr x%x\n",uiErr);

    (void)FMU_IsrHandler((FMUFaultid_t)FMU_ID_SYS_SM_CFG, (FMUSeverityLevelType_t)FMU_SVL_LOW, NULL, NULL );
    DSE_Deinit();

#else
    mcu_printf("MCU_BSP_SUPPORT_DRIVER_FMU flag is not set to 1. Please check the build flag.\n");
#endif  // defined( MCU_BSP_SUPPORT_DRIVER_FMU ) && ( MCU_BSP_SUPPORT_DRIVER_FMU == 1 )

    return;
}

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_DSE == 1 )

