/*
***************************************************************************************************
*
*   FileName : eth_test.c
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#if ( MCU_BSP_SUPPORT_APP_SAMPLE_ETH == 1 )

#include <eth_test.h>
#ifdef OS_FREERTOS
#include "FreeRTOS.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_IP.h"
#endif

/*
***************************************************************************************************
*                                          ETH_Test
* Function to test loopback via console command.
*
* @param    uiMode [in]       : Test mode
* @return
* Notes
*
***************************************************************************************************
*/

void ETH_TestSend
(
    uint32                              uiIdx
)
{
    volatile const uint8                    test_tx1[64] =
    {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, // dst addr 6 byte
        0x00, 0x12, 0x34, 0x56, 0x78, 0x90, // src addr 6 byte
        0x08, 0x06,                         // Ether type 2 byte
        0x00, 0x01, 0x08, 0x00, 0xaa, 0xaa, 0xaa, 0xaa, //data
        0x00, 0x12, 0x34, 0x56, 0xbb, 0xbb, 0xbb, 0xbb,
        0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xc0, 0xa8, 0x01, 0x64,
    };

    volatile const uint8                    test_tx2[128] =
    {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, // dst addr 6 byte
        0x00, 0x12, 0x34, 0x56, 0x78, 0x90, // src addr 6 byte
        0x08, 0x06,                         // Ether type 2 byte
        0x00, 0x01, 0x08, 0x00, 0xcc, 0xcc, 0xcc, 0xcc, //data
        0x00, 0x12, 0x34, 0x56, 0xdd, 0xdd, 0xdd, 0xdd,
        0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xc0, 0xa8, 0x01, 0x64,
    };

    volatile const uint8                    test_tx3[256] =
    {
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, // dst addr 6 byte
        0x00, 0x12, 0x34, 0x56, 0x78, 0x90, // src addr 6 byte
        0x08, 0x06,                         // Ether type 2 byte
        0x00, 0x01, 0x08, 0x00, 0xee, 0xee, 0xee, 0xee, //data
        0x00, 0x12, 0x34, 0x56, 0xff, 0xff, 0xff, 0xff,
        0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xc0, 0xa8, 0x01, 0x64,
    };

    switch(uiIdx)
    {
        case 0:
            ETH_Send((uint8 *)test_tx1 , sizeof(test_tx1));
            break;
        case 1:
            ETH_Send((uint8 *)test_tx2 , sizeof(test_tx2));
            break;
        case 2:
            ETH_Send((uint8 *)test_tx3 , sizeof(test_tx3));
            break;
        default :
            break;
    }
}

#define SIZEOFMACADDR                   (6)
#define SIZEOFIPV4ADDR                  (4)

void ETH_TestInit
(
    uint32                              uiMode
)
{
    ETH_CreateRecvTask(uiMode);
}

void ETH_TestIPInit
(
    void
)
{
    uint8 ucMacAddress[SIZEOFMACADDR] = {0, };
    uint8 ucIpAddress[SIZEOFIPV4ADDR] = {0, };
    uint8 ucNetMask[SIZEOFIPV4ADDR] = {255, 255, 255, 0}; // C Class For Testing.
    uint8 ucGatewayAddress[SIZEOFIPV4ADDR] = {192, 168, 1, 1 };
    uint8 ucDNSServerAddress[SIZEOFIPV4ADDR] = {10, 10, 10, 1};
    uint8 ucTargetIpAddress[SIZEOFIPV4ADDR] = {192, 168, 1, 2};
    uint32_t ulIPAddress;

    ucMacAddress[0] = 0x00;
    ucMacAddress[1] = 0x11;
    ucMacAddress[2] = 0x22;
    ucMacAddress[3] = 0x33;
    ucMacAddress[4] = 0x44;
    ucMacAddress[5] = 0x55;

    ucIpAddress[0] = 192;
    ucIpAddress[1] = 168;
    ucIpAddress[2] = 1;
    ucIpAddress[3] = 102;

    ucGatewayAddress[0] = ucIpAddress[0];
    ucGatewayAddress[1] = ucIpAddress[1];
    ucGatewayAddress[2] = ucIpAddress[2];
    ucGatewayAddress[3] = 1;

    ucTargetIpAddress[0] = 192;
    ucTargetIpAddress[1] = 168;
    ucTargetIpAddress[2] = 1;
    ucTargetIpAddress[3] = 2;


    FreeRTOS_IPInit(ucIpAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, ucMacAddress);

    // wait for 1 second
    vTaskDelay(5000);

    ulIPAddress = FreeRTOS_inet_addr_quick(ucTargetIpAddress[0], ucTargetIpAddress[1], ucTargetIpAddress[2], ucTargetIpAddress[3]);
    for (int i = 0; i < 10; i++){
        mcu_printf("Send Ping Request to %d.%d.%d.%d\n", ucTargetIpAddress[0], ucTargetIpAddress[1], ucTargetIpAddress[2], ucTargetIpAddress[3]);
        FreeRTOS_SendPingRequest(ulIPAddress, 8, 1000);
        vTaskDelay(1000);
    }
}

void vApplicationPingReplyHook( ePingReplyStatus_t eStatus, uint16_t usIdentifier )
{
    if(eStatus == eSuccess)
    {
        mcu_printf("Ping Reply Success\n");
    }
    else
    {
        mcu_printf("Ping Reply Fail\n");
    }
}

void ETH_TestIperf
(
    void
)
{
    vIPerfInstall();
}
void ETH_TestLoopback
(
    uint32                              uiMode
)
{
    ETH_CreateRecvTask(uiMode);
}

#endif  // ( MCU_BSP_SUPPORT_APP_SAMPLE_ETH == 1 )

