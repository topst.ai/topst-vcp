/*
***************************************************************************************************
*
*   FileName : idle.c
*
*   Copyright (c) Telechips Inc.
*
*   Description : Idle state
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#if ( MCU_BSP_SUPPORT_APP_IDLE == 1 )

#include <app_cfg.h>

#include "bsp.h"
#include "idle.h"


/**************************************************************************************************
*                                  STATIC FUNCTIONS DECLARATION
**************************************************************************************************/

static void IdleTask
(
    void *                               pArg
);

/**************************************************************************************************
*                                        STATIC FUNCTIONS
**************************************************************************************************/

static void Idle
(
    void
)
{
    while(1);
}

static void IdleTask
(
    void *                               pArg
)
{
    (void)pArg;

    Idle();
}

/**************************************************************************************************
*                                             FUNCTIONS
**************************************************************************************************/

void IDLE_CreateTask(void)
{
    static uint32   IdleTaskID = 0UL;
    static uint32   IdleTaskStk[32UL];

    (void)SAL_TaskCreate
    (
        &IdleTaskID,
        (const uint8 *)"Idle",
        (SALTaskFunc)&IdleTask,
        &IdleTaskStk[0],
        32UL,   //task stack size
        SAL_PRIO_LOWEST,
        NULL
    );
}

#endif  // ( MCU_BSP_SUPPORT_APP_IDLE == 1 )

