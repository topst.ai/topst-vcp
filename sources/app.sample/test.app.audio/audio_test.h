/*
***************************************************************************************************
*
*   FileName : audio_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_AUDIO_TEST_HEADER
#define MCU_BSP_AUDIO_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_AUDIO == 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_I2S != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_I2S value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_EFLASH != 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_I2C != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_I2C value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_EFLASH != 1 )

/**************************************************************************************************
*                                             INCLUDE FILES
**************************************************************************************************/
#include <sal_internal.h>

#include <app_cfg.h>
#include <i2s.h>
#include <gic_enum.h>
#include <gic.h>
#include <debug.h>

#ifdef WM8731
    #include <wm8731.h>
#else
    #include <wm8904.h>
#endif

typedef enum AUDIOStatus
{
    I2S_ERROR                           = 0,
    I2S_DONE,
    I2S_READY,
    I2S_PLAYBACK,
    I2S_PAUSE,
    I2S_RESUME
} AUDIOStatus_t;


/**************************************************************************************************
*                                             DEFINITIONS
**************************************************************************************************/
typedef struct AUDIOConfig
{
    I2SConfig_t *                       acI2sCfg;
    uint8                               acCodecCh;
} AUDIOConfig_t;


/**************************************************************************************************
*                                        FUNCTION PROTOTYPES
**************************************************************************************************/
void AUDIO_TestUsage
(
    void
);

void AUDIO_SampleTest
(
    uint8                               ucArgc,
    void *                              pArgv[]
);

void AUDIO_CreateAppTask
(
    AUDIOConfig_t *                     psAudioCfg
);

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_AUDIO == 1 )

#endif  // MCU_BSP_AUDIO_TEST_HEADER

