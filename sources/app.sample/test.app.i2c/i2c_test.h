/*
***************************************************************************************************
*
*   FileName : i2c_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_I2C_TEST_HEADER
#define MCU_BSP_I2C_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_I2C == 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_I2C != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_I2C value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_I2C != 1 )

/*
***************************************************************************************************
*                                             INCLUDE FILES
***************************************************************************************************
*/
#include <i2c_reg.h>

/*
***************************************************************************************************
*                                             DEFINITIONS
***************************************************************************************************
*/
#define I2C_TEST_PORT                   (0UL)

#define I2C_TEST_CLK_RATE_100           (100)
#define I2C_TEST_CLK_RATE_400           (400)

#define I2C_TEST_MASTER_CH              (0)
#define I2C_TEST_SLAVE_CH               (0)

/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/

/*
***************************************************************************************************
*                                          I2C_TestMain
*
* Function to call all of test functions.
*
* @return
* Notes
*
**************************************************************************************************
*/

void I2C_TestMain
(
    void
);

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_I2C == 1 )

#endif  // MCU_BSP_I2C_TEST_HEADER

