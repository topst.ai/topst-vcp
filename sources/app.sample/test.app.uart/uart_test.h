/*
***************************************************************************************************
*
*   FileName : uart_test.h
*
*   Copyright (c) Telechips Inc.
*
*   Description : Demo Application for console function
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef  MCU_BSP_UART_TEST_HEADER
#define  MCU_BSP_UART_TEST_HEADER

#if ( MCU_BSP_SUPPORT_TEST_APP_UART == 1 )

#if ( MCU_BSP_SUPPORT_DRIVER_UART != 1 )
    #error MCU_BSP_SUPPORT_DRIVER_UART value must be 1.
#endif  // ( MCU_BSP_SUPPORT_DRIVER_UART != 1 )

/*
***************************************************************************************************
*                                             DEFINITIONS
***************************************************************************************************
*/

#define UART_FALSE                      (0UL)
#define UART_TRUE                       (1UL)

#define UART_BASE_ADDR                  (0xA0200000UL)

#define UART_WriteReg(p, a, v)          SAL_WriteReg(v, UART_GET_BASE(p) + (a))
#define UART_ReadReg(p, a)              SAL_ReadReg(UART_GET_BASE(p) + (a))

// UART Register Offset
#define OFFSET_DR                       (0x00UL)        // Data register
#define OFFSET_ECR                      (0x04)          // Error Clear Register
#define OFFSET_CR                       (0x30UL)        // Control register
#define OFFSET_IBRD                     (0x24UL)        // Integer Baud rate register
#define OFFSET_FBRD                     (0x28UL)        // Fractional Baud rate register
#define OFFSET_LCRH                     (0x2cUL)        // Line Control register
#define OFFSET_IMSC                     (0x38UL)        // Interrupt Mask Set/Clear Register

// UART Control Register Bitset
#define CR_RTSEn                        (1UL << 14UL)   // RTS hardware flow control enable
#define CR_RXE                          (1UL << 9UL)    // Receive enable
#define CR_TXE                          (1UL << 8UL)    // Transmit enable
#define CR_LBE                          (1UL << 7UL)    // Loopback enable
#define CR_UARTEN                       (1UL << 0UL)    // UART enable

/*
***************************************************************************************************
*                                           FUNCTION PROTOTYPES
***************************************************************************************************
*/

/*
***************************************************************************************************
*                                       UART_SelectTestCase
*
* @param    test case number
* @return
*
* Notes
*
***************************************************************************************************
*/
void UART_SelectTestCase
(
    uint32 uiTestCase
);

#endif  // ( MCU_BSP_SUPPORT_TEST_APP_UART == 1 )

#endif  // MCU_BSP_UART_TEST_HEADER

