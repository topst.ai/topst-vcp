/*
***************************************************************************************************
*
*   FileName : rtc_dev.h
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#ifndef MCU_BSP_RTC_DEV_HEADER
#define MCU_BSP_RTC_DEV_HEADER

#if ( MCU_BSP_SUPPORT_DRIVER_RTC == 1 )

/*
***************************************************************************************************
*                                             INCLUDE FILES
***************************************************************************************************
*/
#include <rtc.h>
#include <debug.h>

/*
***************************************************************************************************
*                                             DEFINITIONS
***************************************************************************************************
*/

/*RTC EXTRA FEATURE=====================================================*/
#ifdef RTC_CONF_DEBUG_ALONE
    #define RTC_FEATURE_USE_DEBUG_ALONE
#endif

/*=======================================================PMIO EXTRA FEATURE*/


#if defined(RTC_FEATURE_USE_DEBUG_ALONE)  || defined(DEBUG_ENABLE)
    #define RTC_D(fmt, args...)        {mcu_printf("[RTC][%s:%d] " fmt, \
                                                __func__, __LINE__, ## args);}
#else
    #define RTC_D(fmt, args...)
#endif

#define RTC_E(fmt, args...)        {mcu_printf("[RTC][%s:%d] Error ! " fmt, \
                                                __func__, __LINE__, ## args);}


typedef enum
{
    RTC_REG_PROT_TIME_LOCK              = 0,
    RTC_REG_PROT_ALARM_LOCK             = 1,
    RTC_REG_PROT_TIME_RELEASE           = 10,
    RTC_REG_PROT_ALARM_RELEASE          = 11,
} RTCRegProt_t;

/*
***************************************************************************************************
*                                             LOCAL VARIABLES
***************************************************************************************************
*/

/*
***************************************************************************************************
*                                         FUNCTION PROTOTYPES
***************************************************************************************************
*/

#endif  // ( MCU_BSP_SUPPORT_DRIVER_RTC == 1 )

#endif  // MCU_BSP_RTC_DEV_HEADER

