/*
***************************************************************************************************
*
*   FileName : preload.c
*
*   Copyright (c) Telechips Inc.
*
*   Description :
*
*
***************************************************************************************************
*
*   TCC Version 1.0
*
*   This source code contains confidential information of Telechips.
*
*   Any unauthorized use without a written permission of Telechips including not limited to
*   re-distribution in source or binary form is strictly prohibited.
*
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty of
*   merchantability, fitness for a particular purpose or non-infringement of any patent, copyright
*   or other third party intellectual property right. No warranty is made, express or implied,
*   regarding the information's accuracy,completeness, or performance.
*
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement between
*   Telechips and Company.
*   This source code is provided "AS IS" and nothing contained in this source code shall constitute
*   any express or implied warranty of any kind, including without limitation, any warranty
*   (of merchantability, fitness for a particular purpose or non-infringement of any patent,
*   copyright or other third party intellectual property right. No warranty is made, express or
*   implied, regarding the information's accuracy, completeness, or performance.
*   In no event shall Telechips be liable for any claim, damages or other liability arising from,
*   out of or in connection with this source code or the use in the source code.
*   This source code is provided subject to the terms of a Mutual Non-Disclosure Agreement
*   between Telechips and Company.
*
***************************************************************************************************
*/

#if ( MCU_BSP_SUPPORT_DRIVER_PRELOAD == 1 )

#include <debug.h>
#include <stdlib.h>
#include <sal_internal.h>

#include <bsp.h>
#include <mpu.h>
#include "preload.h"

extern uint32 __PRECODE_START_LOAD;
extern uint32 __PRECODE_RAM_START__;
extern uint32 __PRECODE_SIZE__;

void PRELOAD_loadOnRam(void)
{
    uint32 rompos         = (uint32)&__PRECODE_START_LOAD;
    uint32 rampos         = (uint32)&__PRECODE_RAM_START__;
    uint32 sfmc_code_end  = (uint32)&__PRECODE_SIZE__;
    uint32 readedvalue;

    while(rampos < sfmc_code_end)
    {
        readedvalue = SAL_ReadReg(rompos);
        SAL_WriteReg(readedvalue, rampos);

        if(rompos < 0x10000000u)//to prevent cert_int30_c_violation
        {
            rompos = rompos + 4u;
        }
        rampos = rampos + 4u;
     }

     return;

}

void PRELOAD_JOB
(
    void
)
{
    /*
       Change the PLL rate for low current
    */

    uint32 uiRegDt;

    //HSM_CLK to XIN
    SAL_WriteReg(0x0UL, 0xA0F2401CUL);

    //CPU/BUS/EFL_CLK to XIN
    SAL_WriteReg(0x0UL, 0xA0F24020UL);

    //SFMC_CLK to XIN
    uiRegDt = (SAL_ReadReg(0xA0F24028UL) & 0x9FFFFFFFUL);
    SAL_WriteReg(uiRegDt, 0xA0F24028UL);

    uiRegDt = (SAL_ReadReg(0xA0F24028UL) & 0xE0FFF000UL);
    uiRegDt |= (5UL<<24UL);
    SAL_WriteReg(uiRegDt, 0xA0F24028UL);

    uiRegDt = (SAL_ReadReg(0xA0F24028UL) | (3UL<<29UL));
    SAL_WriteReg(uiRegDt, 0xA0F24028UL);



    //Disable SRC_CLK_DIV
    SAL_WriteReg(0x0UL, 0xA0F24018UL);

    // Drop  PLL0 : 1200 -> 600  MHz
    uiRegDt = (SAL_ReadReg(0xA0F24000UL) & 0xFFFC7FFF);
    uiRegDt |= (0x1UL<<15UL);
    SAL_WriteReg(uiRegDt, 0xA0F24000UL);

    //Check to start PLL0
    while( (SAL_ReadReg(0xA0F24000UL) & (0x1UL<<31UL)) == 0UL)
    {
       BSP_NOP_DELAY();
    }

    // Drop  PLL1 : 1500 -> 750  MHz
    uiRegDt = (SAL_ReadReg(0xA0F2400CUL) & 0xFFFC7FFF);
    uiRegDt |= (0x1UL<<15UL);
    SAL_WriteReg(uiRegDt, 0xA0F2400CUL);

    //Check to start PLL1
    while( (SAL_ReadReg(0xA0F2400CUL) & (0x1UL<<31UL)) == 0UL)
    {
       BSP_NOP_DELAY();
    }

    //Enable SRC_CLK_DIV
    SAL_WriteReg(0x81818100UL, 0xA0F24018UL);

    //Check to start PLL0/1 XIN Div
    while( (SAL_ReadReg(0xA0F24018UL) & ((0x1UL<<30UL)|(0x1UL<<22UL)|(0x1UL<<14UL))) != 0UL)
    {
       BSP_NOP_DELAY();
    }




    // Reset HSM rate (200 -> 187.5 MHz)
    uiRegDt = ((2<<4UL)|(1<<0UL));
    SAL_WriteReg(uiRegDt, 0xA0F2401CUL);

    //Check to start HSM
    while( (SAL_ReadReg(0xA0F2401CUL) & (0x1UL<<7UL)) != 0UL)
    {
       BSP_NOP_DELAY();
    }

    // Reset CPU/BUS/EFL rate (300   / 187.5 / 150   MHz)
    uiRegDt  = ((1 << 28UL) | (1 << 24UL)); //EFL
    uiRegDt |= ((2 << 12UL) | (1 << 8UL)); //BUS
    uiRegDt |= ((1 <<  4UL) | (0 << 0UL)); //CPU
    SAL_WriteReg(uiRegDt, 0xA0F24020UL);

    //Check to start CPU/BUSS/EFL
    while( (SAL_ReadReg(0xA0F24020UL) & ((0x1UL<<31UL)|(0x1UL<<15UL)|(0x1UL<<7UL))) != 0UL)
    {
       BSP_NOP_DELAY();
    }

    // Reset SFMC rate (83.3)
    uiRegDt = (SAL_ReadReg(0xA0F24028UL) & 0x9FFFFFFFUL);
    SAL_WriteReg(uiRegDt, 0xA0F24028UL); //disable

    uiRegDt = (SAL_ReadReg(0xA0F24028UL) & 0xE0FFF000UL);
    uiRegDt |= ((1UL<<24UL)|(8UL<<0UL));
    SAL_WriteReg(uiRegDt, 0xA0F24028UL); //pll1/div

    uiRegDt = (SAL_ReadReg(0xA0F24028UL) | (3UL<<29UL));
    SAL_WriteReg(uiRegDt, 0xA0F24028UL); //enable

    //Check to start SFMC
    while( (SAL_ReadReg(0xA0F24028UL) & (0x1UL<<31UL)) == 0UL)
    {
       BSP_NOP_DELAY();
    }
    return;
}

#endif  // ( MCU_BSP_SUPPORT_DRIVER_PRELOAD == 1 )

